using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using prueba.Models;
using System.Net.Mail;
using Newtonsoft.Json.Linq;

namespace prueba.Controllers
{
    [Route("api/[Action]")]
    public class AuthController : Controller
    {

        public AuthController(
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager,
            IConfiguration configuration
            )
        {
            _usrMng = userManager;
            _singMng = signInManager;
            _config = configuration;
        }

        //variables para admin de identityusers, sing-in y configuraciones
        private readonly SignInManager<IdentityUser> _singMng;
        private readonly UserManager<IdentityUser> _usrMng;
        private readonly IConfiguration _config;
        
        /**
        Registra un usuario
        Input: Objeto de la siguiente manera
            {
                "Email": string,
                "Password": string,
                "UserName": string,
                "PhoneNumber": string
            }
        Output: 
            string de JWT
         */
         //POST api/Register
        [AllowAnonymous]
        [HttpPost]
        public async Task<object> Register([FromBody]User data)
        {
            //llenar datos de IdentityUser
            var idtyUsr = new IdentityUser{
                UserName = data.UserName, 
                Email = data.Email,
                PhoneNumber = data.PhoneNumber
            };

            var res = await _usrMng.CreateAsync(idtyUsr, data.Password);
            if(res.Succeeded){
                await _singMng.SignInAsync(idtyUsr, false);
                return await GenerateJwtToken(data.Email, idtyUsr);
            }
            //definitivamente hubiera estado mejor que regresara jsons, 
            //pero por cuestiones de tiempo ya no me dio tiempo de hacerlo
            return "No fue posible registrar el usuario dado";
        }
        
        //POST api/Login
        /***
        Iniciar sesion con un usuario
        Input: Objeto (User) de la siguiente manera
            {
                "Email": string,
                "Password": string
            }
        Output: 
            string de JWT
         */
        [AllowAnonymous]
        [HttpPost]
        public async Task<object> Login([FromBody] LgnFormData data)
        {
            var res = await _singMng.PasswordSignInAsync(data.Email, data.Password, false, false);
            
            if (res.Succeeded)
            {
                var usr = _usrMng.Users.SingleOrDefault(x => x.Email == data.Email);
                return await GenerateJwtToken(data.Email, usr);
            }
            
            return "Usuario o contraseña incorrecta";
        }

        /***
        Genearadora de JWT, de acuerdo a los datos del usuario 
        y los keys que se tienen en la configuracion
         */
        private async Task<object> GenerateJwtToken(string email, IdentityUser user)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_config["JwtExpireDays"]));

            var token = new JwtSecurityToken(
                _config["JwtIssuer"],
                _config["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        //POST api/Olvide/Contrasenia?email=CORREO
        /***
        Envia un correo a la direccion dada para recuperacion de contrasenia
        Input: 
            String como parametro en la URL llamado email
        Output
            Mensaje de estatus de la operacion
         */
        [HttpPost]
        public async Task<Object> OlvideContrasenia(string email){
            
            // return email;

            IdentityUser user = _usrMng.Users.SingleOrDefault(x => x.Email == email);
            if(user == null){
                return "Email no registrado";
            }

            var token = await _usrMng.GeneratePasswordResetTokenAsync(user);
            var callbackUrl = Url.Action(
                action: "ResetPassword", 
                values: new {codigo = token }
                );
            //construir completamente una ruta ya que Url.Action dio batalla para dar la ruta completa, opte por esta solucion
            string fullURL = this.Request.Scheme + "://" + this.Request.Host + this.Request.PathBase + callbackUrl;
            this.sendMail(user.Email, "Reestablecer contrasenia", "Reestablezca su contrasenia haciendo click en el siguiente enlace:\n" + fullURL );
            return "Correo enviado correctamente";
        }

        /***
        Enviar correo
        Inputs:
            string emailTo: direccion a donde enviar,
            string subject: Asunto del correo,
            string body: Cuerpo del correo
         */
        private void sendMail(string emailTo, string subject, string body){
            //configurable desde las variables de configuracion en appsettings.json
            SmtpClient client = new SmtpClient(_config["smtpMailHost"], int.Parse(_config["portHost"]));
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(_config["outComeEmail"], _config["outComeEmailPass"]);
            client.EnableSsl = true;
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(_config["outComeEmail"]);
            mailMessage.To.Add(emailTo);
            mailMessage.Body = body;
            mailMessage.Subject = subject;
            client.Send(mailMessage);
        }
        
        
        //POST api/Olvide/ResetPassword?codigo=CODIGO
        /**
        Reestablecer una contrasenia, debera enviar el token anteriormente 
        obtenido en el metodo "OlvideContrasenia".
        Input:
            codigo: Como variable desde la URL.
            Objeto de la siguiente manera
            {
                string "Email":  Correo al que se le cambiara la contrasenia,
                string "Password": Nueva contrasenia que establecer
            }
        Output:
            string: Mensaje de estatus
         */
        [HttpPost]
        public async Task<Object> ResetPassword(string codigo, [FromBody]LgnFormData data){
            
            IdentityUser user = _usrMng.Users.FirstOrDefault(x => x.Email == data.Email);
            user.PasswordHash = _usrMng.PasswordHasher.HashPassword(user, data.Password);
            var res = await _usrMng.UpdateAsync(user);
            var msg = res.Succeeded ? "Contrasenia actualizada exitosamente": "Error actualizando contrasenia";
            return msg;
        }

        /**
        Clase para efectuar algunos movimientos en los metodos de arriba
         */
        public class LgnFormData
        {
            [Required]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = "La contrasenia debera tener al menos 7 caracteres", MinimumLength = 7)]
            public string Password { get; set; }

        }
    }
}
