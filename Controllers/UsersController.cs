﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using prueba.Models;
using Newtonsoft.Json.Linq;

namespace prueba.Controllers
{
    /***
    IMPORTANTE: Debera proporcionar en sus Headers, el header:
    Authorizacion: Bearer JWT-GENERADO
    para poder ejecutar cualquier metodo de este controller
     */
    [Authorize]
    [Route("api/users")]
    public class UsersController : Controller
    {

        public UsersController(
            UserManager<IdentityUser> userManager
            )
        {
            _usrMng = userManager;
        }
        private readonly UserManager<IdentityUser> _usrMng;
        
        // GET api/users
        /**
        Obtener todos los usuarios
        Output: 
            List<IdentityUser>: Lista de todos los IdentityUser en la DB
         */
        [HttpGet]
        public IEnumerable<IdentityUser> Get()
        {
            return _usrMng.Users.ToList();
        }

        // GET api/users/id
        /**
        Consulta un usuario{
	"Email": "juan.six1@mailinator.com",
	"Password": "QWERa1234!",
	"UserName": "juan.six1",
	"PhoneNumber": "8112861985"
}
        Input:
            string Id: id de la tabla AspNetUsers del usuario a consultar
        Output:
            IdentityUser si encuentra usuario, de lo contrario regresa null
         */
        [HttpGet("{id}")]
        public IdentityUser Get(string id)
        {
            return _usrMng.Users.SingleOrDefault(x => x.Id == id);
            // return "value";
        }

        // POST api/users
        // [HttpPost]
        // public void Post([FromBody]string data)
        // {
        //      No se usara este metodo debido a que los usuarios se crean al registrarse
        //      en api/Register
        // }

        // PUT api/users/{ID}
        /**
        Edita un IdentityUser segun su Id y los datos proporcionados
        Inputs:
            string id: id del identityUser a editar
            User user: Datos del usuario para modificar 
            (UserName y PhoneNumber seran unicamente los que se actualizaran)
        Output:
            string: Mensaje de estatus
         */
        [HttpPut("{id}")]
        public async Task<object>  Put(string id, [FromBody]User user)
        {
            //por simplicidad se opta por solo poder editar telefono y username
            IdentityUser userDB = _usrMng.Users.SingleOrDefault(x => x.Id == id);
            string response = "";
            if(userDB != null){
                userDB.UserName = user.UserName;
                userDB.PhoneNumber = user.PhoneNumber;
                await _usrMng.UpdateAsync(userDB);
                response = "Actualizado correctamente";
            }else{
                response = "No se encontro usuario con el id proporcionado";
            }
            return response;
        }

        // DELETE api/users/{ID}
        /**
        Elimina el usuario proporcionado
        Input:
            string id: Id del IdentityUser a eliminar, es parametro de url
        Output:
            string: mensaje de estatus
         */
        [HttpDelete("{id}")]
        public async Task<object> Delete(string id)
        {
            IdentityUser userDB = _usrMng.Users.SingleOrDefault(x => x.Id == id);
            string response = "";
            if(userDB != null){
                response = "Eliminado correctamente";
                await _usrMng.DeleteAsync(userDB);
            }else{
                response = "No se encontro usuario con el id proporcionado";
            }
            return response;
        }

    }
}
