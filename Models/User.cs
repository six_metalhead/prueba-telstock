namespace prueba.Models{
    /***
    Clase para manejar cuestiones de usuarios
     */
    public class User{
        public string Email { get; set; }
        public string Password { get; set; }    
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
    }
}