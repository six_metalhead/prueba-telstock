
using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace prueba.Models
{
    public class DBContext : IdentityDbContext
    {
        /***
        Administrara la conexion a la DB
         */
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           optionsBuilder.UseMySql(GetConnectionString());
        }
        
        private static string GetConnectionString()
        {
            //modificar a los parametros de su base de datos
            const string databaseName = "prueba";
            const string databaseUser = "root";
            const string databasePass = "root";
            
            return $"Server=localhost;" +
                   $"database={databaseName};" +
                   $"uid={databaseUser};" +
                   $"pwd={databasePass};" +
                   $"pooling=true;";
        }
    }
}