namespace prueba.Models{

using Newtonsoft.Json.Linq;
    public class WSResult{
        public bool error { get; set; }
        public string message { get; set; } 
        public JContainer data { get; set; }

        public WSResult(bool error, string msg, JContainer dta){
            this.error = error;
            this.message = msg;
            this.data = dta;
        }
    }
}